//
//  PreviewViewController.swift
//  customCamera
//
//  Created by Harshvirsinh Parmar on 08/03/22.
//

import UIKit

class PreviewViewController: UIViewController {

    @IBOutlet var cancleButon: UIButton!
    @IBOutlet var sendPhotoButton: UIButton!
    @IBOutlet var Photo: UIImageView!
    var image: UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        Photo.image = self.image
        navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendPhotoButton(_ sender: UIButton) {
    }
    
    @IBAction func cancleButton(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
