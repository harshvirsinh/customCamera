//
//  ViewController.swift
//  customCamera
//
//  Created by Harshvirsinh Parmar on 08/03/22.
//

import UIKit
import AVFoundation
class ViewController: UIViewController {
    var captureSession = AVCaptureSession()
    var avsession: AVCaptureSession?
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var PhotoOutput =  AVCapturePhotoOutput()
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    let device = AVCaptureDevice.default(for: .video)
    var image: UIImage?
    
    private enum FlashPhotoMode {
            case on
            case off
        }
   private enum CameraType {
        case Front
        case Back
    }
    @IBOutlet var captureButton: UIButton!
    @IBOutlet var flashButton: UIButton!
    @IBOutlet var flipCamera: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpCaptureSession()
        setUpCamera()
  
    }
    private func setUpCamera(){
        let session = AVCaptureSession()
        if  let captureDevice = device
        {
            do{
                let input = try AVCaptureDeviceInput(device: captureDevice )
                
                if session.canAddInput(input){
                    session.addInput(input)
                }
                
                if session.canAddOutput(PhotoOutput){
                    session.addOutput(PhotoOutput)
                }
                cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                cameraPreviewLayer?.videoGravity = .resizeAspectFill
                cameraPreviewLayer?.session = session
                cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                cameraPreviewLayer?.frame = self.view.frame
                self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
                session.startRunning()
                self.avsession = session
            }
            catch{
                print("Error Cappturing Photo")
            }
        }
    }
    func setUpCaptureSession(){
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }

    @IBAction func capturephoto(_ sender: UIButton) {
        let settings = AVCapturePhotoSettings()
        PhotoOutput.capturePhoto(with: settings, delegate: self)

    }

    @IBAction func flipCamera(_ sender: UIButton) {
        if let session = avsession {
            guard let currentCameraInput: AVCaptureInput = session.inputs.first else {
                return
            }
            session.beginConfiguration()
            session.removeInput(currentCameraInput)
            
            var newCamera:AVCaptureDevice?
            if let input = currentCameraInput as? AVCaptureDeviceInput {
                if (input.device.position == .back) {
                    newCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
                } else {
                    newCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
                }
            }
            
            guard let newCameras = newCamera else { return }
            self.currentCamera = newCameras
            var err: NSError?
            var newVideoInput: AVCaptureDeviceInput!
            do {
                newVideoInput = try AVCaptureDeviceInput(device: newCameras)
            } catch let err1 as NSError {
                err = err1
                newVideoInput = nil
            }
            
            if newVideoInput == nil || err != nil {
                print("Error creating capture device input: \(err?.localizedDescription ?? "")")
            } else {
                session.addInput(newVideoInput)
            }
            session.commitConfiguration()
        }
    }
    @IBAction func flashButton(_ sender: Any) {
        toggleFlash()
    }
    
    @IBAction func pinchToZoom(_ sender: UIPinchGestureRecognizer) {
        guard let device = device else { return }

               if sender.state == .changed {

                   let maxZoomFactor = device.activeFormat.videoMaxZoomFactor
                   let pinchVelocityDividerFactor: CGFloat = 5.0

                   do {

                       try device.lockForConfiguration()
                       defer { device.unlockForConfiguration() }

                       let desiredZoomFactor = device.videoZoomFactor + atan2(sender.velocity, pinchVelocityDividerFactor)
                       device.videoZoomFactor = max(1.0, min(desiredZoomFactor, maxZoomFactor))

                   } catch {
                       print(error)
                   }
               }
    }
}
extension ViewController: AVCapturePhotoCaptureDelegate{
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData =  photo.fileDataRepresentation(){
            image = UIImage(data: imageData)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PreviewViewController") as! PreviewViewController
            vc.image = self.image
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    //MARK: -FlashMode Code
    private func flashOn(device:AVCaptureDevice)
        {
            do{
                if (device.hasTorch)
                {
                    try device.lockForConfiguration()
                    device.torchMode = .on
                    device.torchMode = .on
                    device.unlockForConfiguration()
                }
            }catch{
                //DISABEL FLASH BUTTON HERE IF ERROR
                print("Device tourch Flash Error ");
            }
        }
    
    private func flashOff(device:AVCaptureDevice)
       {
           do{
               if (device.hasTorch){
                   try device.lockForConfiguration()
                   device.torchMode = .off
                   device.flashMode = .off
                
                   device.unlockForConfiguration()
               }
           }catch{
               //DISABEL FLASH BUTTON HERE IF ERROR
               print("Device tourch Flash Error ");
           }
       }
    func toggleFlash() {
            var device : AVCaptureDevice!

            if #available(iOS 10.0, *) {
                let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera], mediaType: AVMediaType.video, position: .unspecified)
                let devices = videoDeviceDiscoverySession.devices
                device = devices.first!

            } else {
                // Fallback on earlier versions
                device = AVCaptureDevice.default(for: AVMediaType.video)
                
            }

        if ((device as AnyObject).hasMediaType(AVMediaType.video))
            {
                if (device.hasTorch)
                {
                    self.captureSession.beginConfiguration()
                    //self.objOverlayView.disableCenterCameraBtn();
                    if device.isTorchActive == false {
                        self.flashOn(device: device)
                    } else {
                        self.flashOff(device: device);
                    }
                    //self.objOverlayView.enableCenterCameraBtn();
                    self.captureSession.commitConfiguration()
                }
            }
        }
}
